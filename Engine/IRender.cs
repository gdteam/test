﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace RoguelikeGame.Engine {
    public interface IRender {
        /*
        void Clear(BitmapTemplate surface, Color color);
        void DrawSprite(BitmapTemplate surface, BitmapTemplate sprite, int x, int y);
        void DrawSprite(BitmapTemplate surface, BitmapTemplate sprite, Point2D<int> pos);
        void DrawSprite( BitmapTemplate surface, BitmapTemplate sprite, Point2D<int> srcPos, Point2D<int> destPos, int size );
        void Clip( Surface surface, Rectangle clipRect );
        Rectangle GetClippingArea( Surface surface ); */
        void Clear(Color color);
        void DrawSprite(BitmapTemplate sprite, int x, int y);
        void DrawSprite(BitmapTemplate sprite, Point2D<int> pos);
        void DrawSprite( BitmapTemplate sprite, Point2D<int> srcPos, Point2D<int> destPos, int size );
        void Clip( Rectangle clipRect );
        Rectangle GetClippingArea();

    }
}
