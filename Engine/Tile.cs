﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoguelikeGame.Engine {
    public class Tile {
        public const UInt32 TRANSPARENT   = 0x00000001;
        public const UInt32 PASSABLE      = 0x00000002;

        //actual fields
        public uint Flags; //should this be private?
        //public int Image; // ?
        public int Type; //TODO: Заменить на enum.
        public BaseItem Item;
        public Creature Monster;

        //helper methods
        public bool Passable() {
            return ( Flags & PASSABLE ) != 0;
        }
        
        public bool Transparent() {
            return ( Flags & TRANSPARENT ) != 0;
        }
    }
}
