﻿/*
 * Created by SharpDevelop.
 * User: Archangel Nanael
 * Date: 18.12.2013
 * Time: 3:22
 */
using System;
using System.Drawing;

namespace RoguelikeGame.Engine
{
    /// <summary>
    /// Description of TileView.
    /// </summary>
    public class TileView
    {
        private const int MAX_TILES = 2;
        private Sprite[] tiles;
        
        // Почему-то работает только с test_sprite_1 и 2.
        // Если что-нибудь переименовать в это, то всё будет хорошо.
        // Если использовать другие имена, то NotFoundException
        // Magically fixed (?)
        private static string prefix = "Resources/Sprites"; 
        private static string[] names = { "grass_1.png", "soil_1.png" };
        
        public TileView()
        {
            tiles = new Sprite[ MAX_TILES ];
            // Считывать имена из файла?
            // Захардкодить?
            for( int i = 0; i < MAX_TILES; ++i ) {
                tiles[ i ] = new Sprite( (Bitmap)Bitmap.FromFile( prefix + "/" + names[ i ] ));
            }
        }
        
        public Sprite GetImage( int tID ) {
            return tiles[ tID ];
        }
    }
}
