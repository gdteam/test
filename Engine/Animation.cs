﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoguelikeGame.Engine {
    class Animation: Sprite {
        const ushort DEFAULT_TIME_SHOWN = 2;
        Sprite[] sprite;
        ushort[] time;
        int current;
        ushort tick;

        public Animation(Sprite[] sprites):base(sprites[0]){
            sprite = sprites;
            time = new ushort[sprites.Length];
            for(int i = 0; i < time.Length; i++){
                time[i] = DEFAULT_TIME_SHOWN;
            }
            Init();
        }

        public Animation(Sprite[] sprites, ushort[] shown):base(sprites[0]) {
            sprite = sprites;
            time = shown;
            Init();
        }

        void Init(){
            current = 0;
            tick = time[0];
        }
        
        public void Update(){
            tick--;
            if (tick <= 0) Next();
        }

        void Next(){
            current++;
            if (current > GetFramesCount() - 1) current = 0;
            tick = time[current];
        }

        public override System.Drawing.Bitmap GetBitmap() {
            return sprite[current].GetBitmap();
        }

        public Animation SetSprite(int index, Sprite sprite){
            if(index < GetFramesCount()){
                this.sprite[index] = sprite;
            }
            return this;
        }

        public Animation SetTime(int index, ushort time){
            if(index < GetFramesCount()){
                this.time[index] = time;
            }
            return this;
        }

        public Animation SetSpriteAndTime(int index, Sprite sprite, ushort time){
            if(index < GetFramesCount()){
                this.sprite[index] = sprite;
                this.time[index] = time;
            }
            return this;
        }

        public int GetFramesCount(){
            return sprite.Length;
        }
    }
}
