﻿/*
 * Created by SharpDevelop.
 * User: Archangel Nanael
 * Date: 16.12.2013
 * Time: 20:38
 */
using System;

namespace RoguelikeGame.Engine
{
    /// <summary>
    /// Description of RandomGenerator.
    /// </summary>
    public class RandomGenerator
    {
        // The implementation of the random number generator in the Random class is not guaranteed
        // to remain the same across major versions of the .NET Framework. As a result, your application
        // code should not assume that the same seed will result in the same pseudo-random sequence
        // in different versions of the .NET Framework. (c) MSDN
        
        //TODO: Написать нормальный генератор.
        private Random rand;
        
        public RandomGenerator() {
            rand = new Random();
        }
        
        public RandomGenerator( int seed ) {
            rand = new Random( seed );
        }
        
        public int Next() {
            return rand.Next();
        }

        public int Next( int max ) {
            return rand.Next( max );
        }
        
        public int Next( int min, int max ) {
            return rand.Next( min, max );
        }

        public int GetSeed() {
            return 0;
        }
        
        public void SetSeed( int seed ) {
            rand = new Random( seed );
        }
        
    }
}
