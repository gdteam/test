﻿/*
 * Created by SharpDevelop.
 * User: Archangel Nanael
 * Date: 17.12.2013
 * Time: 7:11
 */
using System;
using System.Drawing;
using System.Collections.Generic;

namespace RoguelikeGame.Engine
{
    /// <summary>
    /// Description of SceneManager.
    /// </summary>
    
    // Нужна фабрика/пул объектов.
    public class DrawableNode {
        public int X, Y;
        public Sprite Image;        
    }

    class SceneManager
    {
        private const int TILE_SIZE = 32;
        private Map map;
        private Camera mainCamera;
        // list of cameras (?)
        // list of monsters
        // list of items
        
        private IRender renderer;
        private Surface screen;
        private TileView tileView;
        
        public SceneManager( Game game )
        {
            this.renderer = game.renderer; 
            this.screen = game.screen; // Фигня какая-то.
            this.tileView = new TileView(); // FIXME: Это должно быть не здесь.
        }
        
        public void AddMap( Map map ) {
            // Пока так.
            this.map = map;
        }
        
        public Camera CreateCamera() {
            // Пока только одна камера.
            //mainCamera = new Camera( 0, 0, new Rectangle( 0, 0, frmMain.instance.Width, frmMain.instance.Height ), renderer, screen );
            //Bitmap b = screen.GetBitmap();
            //mainCamera = new Camera( 0, 0, new Rectangle( 0, 0, b.Width, b.Height ), renderer, screen );
            mainCamera = new Camera( 0, 0, new Rectangle( 0, 0, 640, 480 ), renderer, screen );
            return mainCamera;
        }

        public void DrawMap() {
            var nodes = new List<DrawableNode>();
            int dh, dw;
            dh = (int)Math.Ceiling( (double)mainCamera.GetHeight() / (double)TILE_SIZE );
            dw = (int)Math.Ceiling( (double)mainCamera.GetWidth() / (double)TILE_SIZE );

            int px = mainCamera.GetX() / TILE_SIZE;
            int py = mainCamera.GetY() / TILE_SIZE;
            int xoffs = mainCamera.GetX() % TILE_SIZE;
            int yoffs = mainCamera.GetY() % TILE_SIZE;

            Tile tile;
            int nx, ny;
            for( int y = 0; y <= dh; ++y ) {
                for( int x = 0; x <= dw; ++x ) {
                    nx = px + x;
                    ny = py + y;
                    if( nx >= 0 && nx < map.Width && ny >= 0 && ny < map.Height ) {
                        var node = new DrawableNode();
                        tile = map.GetTile( nx, ny );
                        node.Image = tileView.GetImage( tile.Type );
                        node.X = x * TILE_SIZE  - xoffs;
                        node.Y = y * TILE_SIZE  - yoffs;
                        nodes.Add( node );
                    }
                }
            }
            mainCamera.Render( nodes ); // Лишнее действие, по-моему.
        }
    }
}
