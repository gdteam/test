﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoguelikeGame.Engine.Input
{
    /// <summary>
    /// Author: Curly brace
    /// Simple scroll camera event
    /// </summary>
    public class ScrollCameraEvent : InputEvent
    {
        public int X { get; set; }
        public int Y { get; set; } 

        public ScrollCameraEvent() { ; }

        public ScrollCameraEvent(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public override Object Clone()
        {
            return new ScrollCameraEvent(X, Y);
        }

        public override void Proceed(Game game)
        {
            game.camera.Scroll(X, Y);
        }
    }
}
