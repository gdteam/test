﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoguelikeGame.Engine.Input
{
    public class InputHandler
    {
        public Dictionary<int, InputEvent> inputEvents;

        private IInput _input;

        public Queue<InputEvent> eventQueue;

        public InputHandler()
        {
            _input = new InputManager();

            inputEvents = new Dictionary<int, InputEvent>();

            eventQueue = new Queue<InputEvent>();
        }

        public void Init()
        {
            inputEvents.Add((int)ConsoleKey.LeftArrow, new ScrollCameraEvent(-5, 0));
            inputEvents.Add((int)ConsoleKey.RightArrow, new ScrollCameraEvent(5, 0));
            inputEvents.Add((int)ConsoleKey.UpArrow, new ScrollCameraEvent(0, -5));
            inputEvents.Add((int)ConsoleKey.DownArrow, new ScrollCameraEvent(0, 5));
        }

        public void Act()
        {
            foreach (var key in inputEvents.Keys)
            {
                if (_input.KeyPressed(key))
                {
                    eventQueue.Enqueue(inputEvents[key].Create(Game.GetTickCount()));
                }
            }
        }
    }
}
