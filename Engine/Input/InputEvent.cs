﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoguelikeGame.Engine.Input
{
    /// <summary>
    /// Author: Curly Brace
    /// This class represents high level abstraction input events (for example - MoveCameraEvent, AttackEvent)
    /// </summary>
    public abstract class InputEvent : ICloneable
    {
        public int KeyCode { get; set; }
        public ulong CreatedGameTic { get; set; }

        /// <summary>
        /// This function serves as factory method
        /// </summary>
        /// <param name="gameTic">Current game tic to store time, when event was created</param>
        /// <returns>return object will be stored in Queue</returns>
        public virtual InputEvent Create(ulong gameTic)
        {
            var inputEvenet = (InputEvent)Clone();
            inputEvenet.CreatedGameTic = gameTic;

            return inputEvenet;
        }

        /// <summary>
        /// Need for factory method
        /// </summary>
        /// <returns>clone</returns>
        public virtual object Clone()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This function will affect game
        /// </summary>
        /// <param name="game"></param>
        public abstract void Proceed(Game game);
    }
}
