﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace RoguelikeGame.Engine {       
    public class Surface: Sprite {
        public Surface(Bitmap bitmap):base(bitmap){}
        public Surface(int x, int y):base(x, y){}
        public Surface(Point2D<int> point):base(point){}
        public Surface(BitmapTemplate bt):base(bt){}

        public override BitmapTemplate Clone() {
            return new Surface(this);
        }
    }
}
