﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace RoguelikeGame.Engine {
    #region old code
    /*
    class RenderManager: IRender {
        
        Rectangle clip;
        public RenderManager(){
        }

        public void DrawSprite(BitmapTemplate surface, BitmapTemplate sprite, int x, int y) {
            Graphics gr = Graphics.FromImage(surface.GetBitmap());
            gr.SetClip( clip );
            gr.DrawImageUnscaled(sprite.GetBitmap(), x, y);
        }

        public void DrawSprite(BitmapTemplate surface, BitmapTemplate sprite, Point2D<int> pos) {
            DrawSprite(surface, sprite, pos.GetX(), pos.GetY());
        }
        
        public void DrawSprite( BitmapTemplate surface, BitmapTemplate sprite, Point2D<int> srcPos, Point2D<int> destPos, int size ) {
            Graphics g = Graphics.FromImage( surface.GetBitmap() );
            if( clip != null ) //fast hack
                g.SetClip( clip );
            g.DrawImage( sprite.GetBitmap(), new Rectangle( destPos.GetX(), destPos.GetY(), size, size ),
                        new Rectangle( srcPos.GetX(), srcPos.GetY(), size, size ), GraphicsUnit.Pixel );
        }
        
        public void Clear(BitmapTemplate surface, Color color) {
            Graphics gr = Graphics.FromImage(surface.GetBitmap());
            gr.Clear(color);
        }
       
        public void Clip( Surface surface, Rectangle clipRect ) {
            //Graphics g = Graphics.FromImage( surface.GetBitmap() );
            //g.SetClip( clipRect );
            clip = new Rectangle( clipRect.X, clipRect.Y, clipRect.Width, clipRect.Height );
        }
       
        public Rectangle GetClippingArea( Surface surface ) {
            //Graphics g = Graphics.FromImage( surface.GetBitmap() );
            //return new Rectangle( (int)g.ClipBounds.X, (int)g.ClipBounds.Y, (int)g.ClipBounds.Width, (int)g.ClipBounds.Height );
            return new Rectangle( clip.X, clip.Y, clip.Width, clip.Height );
        }  
    } */
    #endregion
    class RenderManager : IRender {
        private Graphics gr;
        
        public RenderManager( BitmapTemplate surface ) {
            gr = Graphics.FromImage( surface.GetBitmap() );
        }
        
        public void Clear(Color color) {
            gr.Clear( color );
        }
        
        public void DrawSprite(BitmapTemplate sprite, int x, int y) {
            gr.DrawImageUnscaled( sprite.GetBitmap(), x, y );
        }
        
        public void DrawSprite(BitmapTemplate sprite, Point2D<int> pos) {
            DrawSprite( sprite, pos.GetX(), pos.GetY() );
        }
        
        public void DrawSprite( BitmapTemplate sprite, Point2D<int> srcPos, Point2D<int> destPos, int size ) {
            gr.DrawImage( sprite.GetBitmap(), new Rectangle( destPos.GetX(), destPos.GetY(), size, size ),
                        new Rectangle( srcPos.GetX(), srcPos.GetY(), size, size ), GraphicsUnit.Pixel );

        }
        
        public void Clip( Rectangle clipRect ) {
            gr.SetClip( clipRect );
        }
        
        public Rectangle GetClippingArea() {
            RectangleF bds = gr.ClipBounds; 
            return new Rectangle( (int)bds.X, (int)bds.Y, (int)bds.Width, (int)bds.Height );
        }
    }
}
