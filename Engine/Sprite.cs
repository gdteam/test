﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace RoguelikeGame.Engine {
    public class Sprite: BitmapTemplate {
        public Sprite(Bitmap bitmap){
            this.bitmap = bitmap;
        }

        public Sprite(int x, int y){
            bitmap = new Bitmap(x, y);
        }

        public Sprite(Point2D<int> point){
            bitmap = new Bitmap(point.GetX(), point.GetY());
        }

        public Sprite(BitmapTemplate bt){
            bitmap = bt.GetBitmap();
        }

        public override BitmapTemplate Clone() {
            return new Sprite(this);
        }
    }
}
