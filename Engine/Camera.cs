﻿/*
 * Created by SharpDevelop.
 * User: Archangel Nanael
 * Date: 17.12.2013
 * Time: 21:59
 */
using System;
using System.Drawing;
using System.Collections.Generic;

namespace RoguelikeGame.Engine
{
    /// <summary>
    /// Description of Camera.
    /// </summary>
    public class Camera
    {
        // Позиция левого-верхнего угла.
        //!
        private Point2D<float> worldPos; // TODO: Переделать так, чтобы это была позиция центра экрана.
        private Point2D<float> newPos; //!
        private Point2D<float> vector; //!
        private Rectangle dims; // Viewport. Позиция на форме и размеры.
        private bool needScroll = false;
        
        private IRender renderer;
        private Surface screen;
        
        public int GetWidth() {
            return dims.Width;
        }
        
        public int GetHeight() {
            return dims.Height;
        }

        public Camera( int px, int py, Rectangle vd, IRender renderer, Surface screen )
        {
            worldPos = new Point2D<float>( (float)px, (float)py ); //!
            vector = new Point2D<float>( 0, 0 ); //!
            newPos = worldPos;
            
            dims = new Rectangle( vd.X, vd.Y, vd.Width, vd.Height );
            this.renderer = renderer;
            this.screen = screen;
        }
        

        public void SetVewport( Rectangle n ) {
            dims = new Rectangle( n.X, n.Y, n.Width, n.Height );
        }

        public void SetPosition( int x, int y ) {
            //worldPos.SetX( x );
            //worldPos.SetY( y );
            newPos = new Point2D<float>( (float)x, (float)y ); //!
            float nx, ny; //!
            nx = ( (float)x - worldPos.GetX() ) / 25; //!
            ny = ( (float)y - worldPos.GetY() ) / 25; //!
            //if( Math.Abs( nx ) < 1 ) nx = Math.Sign( nx );
            //if( Math.Abs( ny ) < 1 ) ny = Math.Sign( ny );
            
            vector = new Point2D<float>( nx, ny ); //!
            needScroll = true;
        }

        public void SetPosition( Point2D<int> newPos ) {
            SetPosition( newPos.GetX(), newPos.GetY() );
        }

        /*
        public void SetPosition( GameObject o ) {
            SetPosition( o.X, o.Y );
        }*/

        public int GetX(){
            return (int)worldPos.GetX(); //!
        }
        
        public int GetY() {
            return (int)worldPos.GetY(); //!
        }
        
        public void Scroll( int dx, int dy ) { // temporary
            worldPos.SetX( worldPos.GetX() + (float)dx ); // ... //!
            worldPos.SetY( worldPos.GetY() + (float)dy ); //!
        }

       public void Update( /* time? */ ) {
            if( needScroll ) {
                /*
                if( Math.Abs( worldPos.GetX() - newPos.GetX() ) < vector.GetX() ) {
                    worldPos.SetX( newPos.GetX() );
                    vector.SetX( 0 );
                }
    
                if( Math.Abs( worldPos.GetY() - newPos.GetY() ) < vector.GetY() ) {
                    worldPos.SetY( newPos.GetY() );
                    vector.SetY( 0 );
                }*/
                double d = distance( worldPos, newPos );
                double l = length( vector );
                if( d <= l ) {
                    vector.SetX( 0 );
                    vector.SetY( 0 );
                    worldPos = newPos;
                    needScroll = false;
                } else {
                    worldPos.SetX( worldPos.GetX() + vector.GetX() );
                    worldPos.SetY( worldPos.GetY() + vector.GetY() );
                }
            }
        }
        
        // Temporary helper functions
        // move it to somewhere
        public static double distance( Point2D<float> p1, Point2D<float> p2 ) { //!
            double dx = (double)p1.GetX() - (double)p2.GetX();
            double dy = (double)p1.GetY() - (double)p2.GetY();
            return Math.Sqrt( dx * dx + dy * dy );
        }
        
        public static double length( Point2D<float> v ) { //!
            return Math.Sqrt( (double)v.GetX() * (double)v.GetX() + (double)v.GetY() * (double)v.GetY() );
        }
        // ---
        
        public void Render( List<DrawableNode> list ) {
            //Rectangle clip = renderer.GetClippingArea( screen );
            Rectangle clip = renderer.GetClippingArea();
            //renderer.Clip( screen, dims );
            renderer.Clip( dims );

            foreach( var i in list ) {
                //renderer.DrawSprite( screen, i.Image, i.X + dims.X, i.Y + dims.Y );
                renderer.DrawSprite( i.Image, i.X + dims.X, i.Y + dims.Y );
            }
            //renderer.Clip( screen, clip );
            renderer.Clip( clip );
        }
    }
}
