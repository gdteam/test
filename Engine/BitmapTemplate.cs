﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace RoguelikeGame.Engine {
    public abstract class BitmapTemplate {
        protected Bitmap bitmap;

        public virtual Bitmap GetBitmap(){
            return bitmap;
        }

        public Point2D<int> GetSize(){
            return new Point2D<int>(bitmap.Size.Width, bitmap.Size.Height);
        }

        public abstract BitmapTemplate Clone();
    }
}
