﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoguelikeGame.Engine {
    public class Point2D<T> {
        T x, y;

        public Point2D(T x, T y){
            this.x = x;
            this.y = y;
        }

        public void SetX( T x ){
            this.x = x;
        }

        public void SetY( T y ){
            this.y = y;
        }
        
        public T GetX(){
            return x;
        }

        public T GetY(){
            return y;
        }
    }
}
