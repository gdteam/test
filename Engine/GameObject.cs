﻿/*
 * Created by SharpDevelop.
 * User: Archangel Nanael
 * Date: 26.12.2013
 * Time: 8:50
 */
using System;

namespace RoguelikeGame.Engine
{
    /// <summary>
    /// Description of GameObject.
    /// </summary>
    public abstract class GameObject
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Type { get; set; } // TODO: Заменить на enum 
        
        public GameObject()
        {
        }
        
        public abstract void Update();
        
    }
}
