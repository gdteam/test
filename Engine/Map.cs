﻿/*
 * Created by SharpDevelop.
 * User: Archangel Nanael
 * Date: 15.12.2013
 * Time: 4:17
 */
using System;

namespace RoguelikeGame.Engine
{
    /// <summary>
    /// Содержит информацию о карте уровня.
    /// </summary>
    public class Map {
        
        // private fields
        private Tile[] _lvl;
        private RandomGenerator rand; // temp
        
        // public fields
        public int Width { get; private set; }
        public int Height { get; private set; }
        
        public Map( int width, int height ) {
            rand = new RandomGenerator( 0x0BA32107 ); //FIXME: Удалить seed.
            this.Width = width;
            this.Height = height;
            
            _lvl = new Tile[ width * height ];
            for( int i = 0; i < width * height; ++i ) {
                _lvl[ i ] = new Tile();
            }
        }
        
        /// <summary>
        /// Генерирует карту.
        /// Временная штука. Не использовать.
        /// Нам нужен MapGeneratorFactory, который будет генерировать генераторы для генерации мира и возвращать IMapGenerator.
        /// </summary>
        public void Generate( /* params */ ) {
            for( int i = 0; i < Width * Height; ++i ) {
                _lvl[ i ].Flags = Tile.PASSABLE; // + Tile.TRANSPARENT;
                _lvl[ i ].Type = rand.Next( 2 );
            }
        }
        
        public Tile GetTile( int x, int y ){
            return _lvl[ x + y * Width ];
        }
    }
}
