﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace RoguelikeGame.Engine {
    class InputManager: IInput {
        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        public static extern short GetAsyncKeyState(Int32 vKey);

        public bool KeyPressed(int keycode) {
            return GetAsyncKeyState(keycode) != 0;
        }
    }
}
