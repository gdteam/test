﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Runtime.InteropServices;
using System.Drawing;
using RoguelikeGame.Engine.Input;

namespace RoguelikeGame.Engine {
    public class Game {
#region GameLoop (DO NOT CHANGE!)
        [DllImport("kernel32.dll", CharSet=CharSet.Auto, ExactSpelling=true)]
        public static extern ulong GetTickCount();
        [DllImport("kernel32.dll")]
        public static extern void Sleep(int msec);

        const int TICKS_PER_SECOND = 25;
        const int SKIP_TICKS = 1000 / TICKS_PER_SECOND;
        const int MAX_FRAMESKIP = 5;
        ulong next_game_tick = GetTickCount();
        int loops;
        float ip;

        private void GameLoop(object game){
            if(!GlobalValues.LimitedFPS){
                while(Loop){
                    loops = 0;
                    while(GetTickCount() > next_game_tick && loops < MAX_FRAMESKIP){
                        UpdateGame((Game)game);
                        next_game_tick += SKIP_TICKS;
                        loops += 1;
                    }

                    ip = (GetTickCount() + SKIP_TICKS - next_game_tick) / SKIP_TICKS;
                    UpdateScreen((Game)game, ip);
                }
            }else{
                while(Loop){
                    UpdateGame( (Game)game);                
                    ip = (GetTickCount() + SKIP_TICKS - next_game_tick) / SKIP_TICKS; //srsly, man
                    UpdateScreen((Game)game, ip);
                    Sleep(40);
                }
            }
        }
#endregion
        private bool Loop;
        public bool Paused = false;
        Thread gamethread;

        public Point2D<int> ScreenSize;
        public Surface screen;

        //Test
        static Sprite warrior_stand = new Sprite((Bitmap)Bitmap.FromFile("Resources/Sprites/warrior_stand_1.png"));
        static Sprite warrior_move_1 = new Sprite((Bitmap)Bitmap.FromFile("Resources/Sprites/warrior_move_1.png"));
        static Sprite warrior_move_2 = new Sprite((Bitmap)Bitmap.FromFile("Resources/Sprites/warrior_move_2.png"));
        static Sprite warrior_attack_1 = new Sprite((Bitmap)Bitmap.FromFile("Resources/Sprites/warrior_attack_1.png"));
        static Sprite warrior_attack_2 = new Sprite((Bitmap)Bitmap.FromFile("Resources/Sprites/warrior_attack_2.png"));
        static Sprite warrior_attack_3 = new Sprite((Bitmap)Bitmap.FromFile("Resources/Sprites/warrior_attack_3.png"));
        static Animation anim_w_move = new Animation(new Sprite[2]{warrior_move_1, warrior_move_2});
        static Animation anim_w_attack = new Animation(new Sprite[3]{warrior_attack_1, warrior_attack_2, warrior_attack_3}).SetTime(2, 5);

        protected InputHandler input;
        public IRender renderer { get; private set; }

        public Game(Point2D<int> ScreenSize){
            this.ScreenSize = ScreenSize;
            screen = new Surface(ScreenSize);
            Init();
        }

        Map map;
        SceneManager sm;
        public Camera camera;
        RandomGenerator rand;
        private void Init(){
            input = new InputHandler();
            input.Init();
            renderer = new RenderManager( screen );

            //test
            map = new Map( 80, 25 );
            map.Generate();
            sm = new SceneManager( this );
            sm.AddMap( map );
            camera = sm.CreateCamera();
            rand = new RandomGenerator();
        }

        public void Start(){
            Loop = true;

            gamethread = new Thread(GameLoop);
            gamethread.Start(this);
        }

        public void Stop(){
            Loop = false;
            gamethread.Abort();
        }

        public void Pause(){
            Paused = true;
        }

        public void Resume(){
            Paused = false;
        }

        ulong lasthit = 0;
        private void UpdateGame(Game game){ //Игровой цикл
            if(!Paused){ // Тут надо ещё проверить, в фокусе ли окно.
                // Обновление игровой ситуации
                camera.Update();

                //Start Changed by Curly Brace
                
                HandleInput();

                //if (input.KeyPressed((int)ConsoleKey.Spacebar) && GetTickCount() - lasthit > 300)
                //{
                //    lasthit = GetTickCount();
                //    camera.SetPosition(rand.Next(75 * 32) - 3 * 32, rand.Next(20 * 32) - 3 * 32);
                //}
                //End Changed by Curly Brace

                anim_w_move.Update();
                anim_w_attack.Update();
            }else{
                // Действия при паузе
            }
            
            frmMain.instance.SetText( 
                                     String.Format( "w: {0}, h: {1}, x: {2}, y: {3}",
                                                          camera.GetWidth(), camera.GetHeight(), camera.GetX(), camera.GetY() ) );
        }

        /// <summary>
        /// Author: Curly Brace
        /// This fucntion handles input inside game loop
        /// </summary>
        private void HandleInput()
        {
            input.Act();

            //I am not sure about this cycle, but i will leave it like that cause i am dumb.
            //Just try to process all events inside 1 tic.
            while (input.eventQueue.Count > 0)
            {
                input.eventQueue.Dequeue().Proceed(this);
            }
        }

        private void UpdateScreen(Game game, float interpolation){ //Вывод на экран            
            //renderer.Clear(screen, Color.DarkSlateGray);
            renderer.Clear(Color.DarkSlateGray);
            //Test
            sm.DrawMap();
            //renderer.DrawSprite(screen, warrior_stand, 50, 50);
            //renderer.DrawSprite(screen, anim_w_move, 100, 50);
            //renderer.DrawSprite(screen, anim_w_attack, 150, 50);
            renderer.DrawSprite(warrior_stand, 50, 50);
            renderer.DrawSprite(anim_w_move, 100, 50);
            renderer.DrawSprite(anim_w_attack, 150, 50);
            //Test
            frmMain.instance.DrawBitmap(screen.GetBitmap());
        }
    }
}
