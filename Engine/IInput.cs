﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoguelikeGame.Engine {
    interface IInput {
        bool KeyPressed(int keycode);
    }
}
