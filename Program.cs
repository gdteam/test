﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using RoguelikeGame.Engine;

namespace RoguelikeGame {
    static class Program {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main(string[] args) {
            for(int i = 0; i < args.Length; i++){
                if (args[i].ToLower() == "--unlim") GlobalValues.LimitedFPS = false;
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmMain());
        }
    }
}
