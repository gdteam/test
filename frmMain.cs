﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using RoguelikeGame.Engine;

namespace RoguelikeGame {
    public partial class frmMain : Form {
        public static frmMain instance; // For tests
        delegate void SetTextCallback(string text);
        delegate void DrawBitmapCallback(Bitmap b);

        Game game;

        public frmMain() {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e) {
            instance = this;
            game = new Game(new Point2D<int>(this.ClientRectangle.Width, this.ClientRectangle.Height));
            game.Start();
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e) {
            game.Stop();
        }

        public void DrawBitmap(Bitmap b){
            if (this.InvokeRequired) {
                DrawBitmapCallback d = new DrawBitmapCallback(DrawBitmap);
                this.Invoke(d, new object[] { b });
            } else {
                this.CreateGraphics().DrawImageUnscaled(b, 0, 0);
            }
        }

        public void SetText(string text) {
            if (this.InvokeRequired) {
                SetTextCallback d = new SetTextCallback(SetText);
                this.Invoke(d, new object[] { text });
            } else {
                this.Text = text;
            }
        }        
    }
}
